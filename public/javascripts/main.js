import $ from "jquery";


(function($){


    function pollForZip(id) {
        $.ajax({
            url: '/pack-is-ready/' + id,
            method: 'GET',
            success: (response) => {
                if (response.status === 404) {
                    setTimeout(function(){
                        pollForZip(id);
                    }, 2000);
                    
                } else if (response.status === 200) {
                    $('.feedback-screen')
                        .addClass('hidden');

                    $('.final-screen')
                        .removeClass('hidden');

                    $('.download')
                        .find('a')
                            .attr('href', response.path);
                }

            }
        });
    }
    setTimeout(function(){
        if ($('.data').length) {
            var data = $('.data').data();
            // data.socketPath && connectIo(data.socketPath);
            data.uploadId && pollForZip(data.uploadId);
        }
    },4000);


    $('.file-select').on('click', function(e) {
        e.preventDefault();
        $('#file').click();
    });

    $('#file').on('change', function() {
        $('.selected-file ').removeClass('hidden');
        $('.form-footer').removeClass('hidden');
        $('.init-file-select').remove();
        $('.file-name').html(this.files[0].name);
    })


    function formatTime(seconds) {
        var minutes = Math.floor(seconds / 60);
        var seconds = seconds % 60;

        return minutes + ':' + (seconds < 10 ?  '0' : '') + seconds;
    }
})($);
