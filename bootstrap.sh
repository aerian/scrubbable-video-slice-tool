#!/usr/bin/env bash



wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -Uvh epel-release-6*.rpm
wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
rpm -Uvh remi-release-6*.rpm

yum -y update

yum install -y gcc php-devel php-pear

yum install -y git-core curl
yum install build-essential libssl-dev

# imagemagick

yum install -y gcc php-devel php-pear
yum install -y ImageMagick ImageMagick-devel

# node, ruby, compass and npm
curl --silent --location https://rpm.nodesource.com/setup | bash -
yum -y install nodejs 
yum -y install ruby
yum -y install ruby-devel
yum -y install rubygems

npm install npm@latest
gem update --system
gem install compass

# Installing n
npm install -g n
n 4.0.0


npm install forever -g
npm install jspm -g
cd /vagrant
npm install
jspm install

export NODE_ENV=production

echo 'share folder contents -----'
ls /vagrant/

# # initialise scrubbable
cp /vagrant/initscrubbabled /etc/init/
chmod a+x /etc/init/initscrubbabled
ln -s /etc/init/initscrubbabled /etc/init.d/initscrubbabled
chkconfig --add initscrubbabled

service initscrubbabled start