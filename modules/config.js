module.exports = {
    title: "Scrubble Video Prep",
    sizes: {
        small: 300,
        medium: 700,
        large: 1000
    },
    error: {
        filetype: 'File must be a video'
    }
}