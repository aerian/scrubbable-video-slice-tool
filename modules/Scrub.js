/**
 * Scrub creator
 *
 * Events:
 * 
 * scrub:start
 * scrub:finished
 * 
 * scrub:responsifyImages:start
 * scrub:responsifyImages:pipe
 * scrub:responsifyImages:finish
 *
 * scrub:archive:start
 * scrub:archive:finish
 *

 */

var split = require('../modules/split.js').split,
    Events = require('events').EventEmitter,
    md5 = require('md5'),
    make_json = require('../modules/add_files_to_config_obj.js').make_json,
    sizing = require('../modules/process_image_size.js'),
    archiver = require('archiver'),
    exec = require('child_process').exec,
    fs = require('fs'),
    schedule = require('node-schedule');



module.exports = (function(process) {
    "use strict";
    class Scrub extends Events{
        init(videoPath) {
            console.log('Scrubifying ' + videoPath);
            this.id = md5(Date.now() + 'Scrubb4L3 M4k3R');
            this.scheduleDelaySeconds = 3600;
            this.videoPath = videoPath;
            this.imagePath = process.cwd() + '/scrub-temp/' + this.id;
            this.archivePath = process.cwd() + '/scrub-archives/' + this.id;
            this.methodPath = ['scheduleCleanup', 'mkdir', 'split', 'makeJson', 'cacheFiles', 'responsifyImages', 'createArchive', 'cleanupImagePath', 'cleanupVideoUpload','finish'];
            this.start = this.start.bind(this);
        }

        getId() {
            return this.id;
        }
        start(videoPath) {
            if (!this.started) {
                this.started = true;
                this.init(videoPath);
                this.emit('scrub:start');
                this.stateIndex = -1;
                this.next = this.next.bind(this);
                this.next();
            }
        }

        mkdir() {
            fs.mkdir(this.archivePath, () => {
                fs.mkdir(this.imagePath, this.next);
            });
        }

        split() {
            split(this.videoPath, this.imagePath, this.next);
        }

        makeJson() {
            make_json(this.imagePath, this.next);
        }
        cacheFiles(files) {
            this.files = files;
            this.next();
        }

        responsifyImages() {
            var i = -1, processFile,
                files = this.files,
                file;
            // this.emit('scrub:responsifyImages:start', {totalImages: files.length});
            this.processTime = Date.now();
            processFile = () => {
                if(this.forceCancel) {
                    i = files.length;
                } else {
                    i++;
                }
                
                if (i < files.length) {
                    file = files[i];
                    if (!file.match(/\.jpg$/)) {
                        throw (file + " is not a jpg");
                        return;
                    };
                    sizing.process(file, this.imagePath, () => {
                        var timeRemaining = this.predictTimeRemaining(files.length - i + 1);
                        // this.emit('scrub:responsifyImages:pipe', {image: i, totalImages: files.length, thumb: this.id + '/small-' + file, timeRemaining: timeRemaining });
                        processFile();
                    });
                } else {
                    // this.emit('scrub:responsifyImages:finish');
                    this.next();
                }
            };
            processFile();
        }

        predictTimeRemaining(itemsRemaining) {
                this.timeGaps =  this.timeGaps || [];
                this.timeGaps.push(Date.now() - this.processTime);

                var gapAverage = this.timeGaps.reduce(function(previousValue, currentValue) {
                                    return previousValue + currentValue;
                                }) / this.timeGaps.length;

                this.processTime = Date.now();
                return Math.round((gapAverage * itemsRemaining) / 1000);
        }

        createArchive() {
            console.log('Saving ' + this.imagePath + ' to ' + this.archivePath);
            // this.emit('scrub:archive:start');
            fs.mkdir(this.archivePath, ()=>{
                console.log('created ' + this.archivePath);
                var output = fs.createWriteStream(this.archivePath + '/scrubbable-pack.zip'),
                    archive = archiver('zip');

                output.on('close', () => {
                    console.log('finished ' + this.archivePath);
                  // this.emit('scrub:archive:finish');
                  this.next();
                });

                output.on('open', () => {
                    console.log('opened ' + this.archivePath);
                    archive.bulk([
                      { expand: true, cwd: this.imagePath, src: ['**'] }
                    ]);

                    archive.pipe(output);
                    archive.on('error', (err) => {
                      throw err;
                    });
                    archive.finalize();
                });
            })
        }

        cancel() {
            console.log('Scrub cancelled.');
            this.fullCleanup('finishForceCancel');
        }

        fullCleanup(finalStep) {
            console.log('fullCleanup started');
            this.stateIndex = -1;
            this.currentProcess && (this.forceCancel = true);
            this.methodPath = ['cleanupImagePath', 'cleanupVideoUpload', 'cleanupArchivePath'];
            finalStep && (this.methodPath.push(finalStep));
            this.currentProcess || this.next();
        }

        finishForceCancel() {
            console.log('Scrub was cancelled');
            this.emit('scrub:finished');
        }
        finishScheduleCleanup() {
            console.log('scheduled cleanup successful');
        }

        cleanupArchivePath() {
            exec('rm -rf ' + this.archivePath, (err) => {

                  console.log('Archive dir cleaned up'); err && console.error(err);
                  this.next();
            })
        }

        cleanupImagePath() {
            exec('rm -rf ' + this.imagePath, (err) => {

                  console.log('Image dir cleaned up'); err && console.error(err);
                  this.next();
            })
        };

        cleanupVideoUpload() {
            fs.unlink(this.videoPath, (err) => {
                err && console.error(err);
                this.next();
            })
        };

        scheduleCleanup() {
            var scheduleTime = new Date((new Date()).setSeconds((new Date()).getSeconds()+this.scheduleDelaySeconds));
            this.cleanupJob = schedule.scheduleJob(scheduleTime, () => {
                console.log('-------SCHEDULED CLEAN UP----------');
                this.fullCleanup('finishScheduleCleanup');
            });
            this.next();
        }

        next() {
            this.stateIndex++;

            if (this.methodPath.length > this.stateIndex) {
                this.currentProcess = this.methodPath[this.stateIndex];
                console.log('Running Method: this.' + this.currentProcess + '();');
                this[this.methodPath[this.stateIndex]].apply(this, arguments);
            } 

        }

        finish() {
            console.log('Scrub is finished');
            this.done = true;
            // this.emit('scrub:finished', this.id + '/scrubbable-pack.zip');
            this.currentProcess = null;
        }
    };

    function bindAll() {
            for (key in this) {
                if (typeof this[key] === 'function') {
                    this[key] = this[key].bind(this);
                }
            }
    }

    return Scrub;

})(process);
