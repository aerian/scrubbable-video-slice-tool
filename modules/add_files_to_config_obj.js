'use strict';
var fs = require('fs'),
    _ = require('underscore');


function make_json(path, callback) {
    var allFiles;
    path = path || '.';
    callback = callback || function() {};
    if (typeof path === 'function') {
        callback = path;
        path = '.';
    }


    function filesToObject(path, callback) {

        fs.readdir(path, function(err, files) {
            console.log(files);
            if (err) {
                console.error('Error in readdir' + err);
            } else {
                allFiles = files;
                var files_in_path = _(files).map(function(file, i) {
                    var position = Number(_(file.match(/\d+/)).first());
                    return {
                        file: file,
                        position: position
                    };
                });
                callback(files_in_path);
            }
        });
    }

    filesToObject(path, function(files) {
        console.log(files.filter((item)=>item.file.match(/\.jpg$/)));
        var fileList = {
            'files': files.filter((item)=>item.file.match(/\.jpg$/))
        };

        fs.open(path + '/config.json', 'wx', function(err, fd) {
            if (err) {
                console.error('Error opening file:' + err);
            } else {
                fs.writeSync(fd, JSON.stringify(fileList, null, 4));
                callback && callback(allFiles);
            }

        });
    });

}

exports.make_json = make_json;
