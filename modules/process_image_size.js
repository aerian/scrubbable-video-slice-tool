var fs = require('fs'),
    _ = require('underscore'),
    gm = require('gm').subClass({ imageMagick: true }),
    path = require('path'),
    config = require('./config.js').sizes;
/**
 * Create options for sizes based on current sizes
 * @param  {[object]} obj with width and height params
 * @return {[object]} sizes
 */

function generateOptionImages(obj) {
    var sizes,
        requiredSizes = _(config).clone();

    sizes = _(requiredSizes).map(function(size, key) {
        var ratio = size / obj.width;
        return {
            label: key,
            width: size,
            height: Math.round(obj.height * ratio)
        };
    });
    return sizes;
}

// function generateNewImages
function processImage(fileName, directory, callback) {
        var filePath, dir;

        dir = typeof directory === 'string' ? directory : './';
        callback = typeof arguments[arguments.length - 1] === 'function' ? arguments[arguments.length - 1] : null;
        filePath = path.join(dir, fileName);
        splitImageAndSaveToDirectory(filePath, dir, true, callback);

}

function splitImageAndSaveToDirectory(fullPathToImage, pathToSave, deleteImageOnceComplete, callback) {
    var fileName = path.basename(fullPathToImage),
        toCompress = [];
    gm(fullPathToImage)
            .size(function(err, value) {
                if (err) {
                    console.log(err);
                    console.trace();
                    return;}
                var newSizes = generateOptionImages(value);

                for (var i = newSizes.length - 1; i >= 0; i--) {
                    var count = 0,
                        size = newSizes[i],
                        pathToNewImage = path.join(pathToSave, size.label + '-' + fileName);


                    this.resize(size.width, size.height)
                        .quality(75)
                        .compress('JPEG')
                        .write(pathToNewImage, function(err) {

                            err && console.log('Error:' + err);



                            count++;
                            if (count == newSizes.length) {
                                deleteImageOnceComplete && fs.unlinkSync(fullPathToImage);

                                callback && callback(arguments);

                            }

                        });
                }
            });
}

exports.process = processImage;
exports.split = splitImageAndSaveToDirectory;
