'use strict';
var exec = require('child_process').exec;

function formatDir(path) {
    //if no path passed we assume they want to place the output in the CWD
    if (typeof path !== 'string') {
        return '.';
    }
    //ensure last character is not a /
    return path.lastIndexOf('/') === path.length - 1 ? path.substr(0, path.length) : path;

}

function split(video, path, callback) {
    var process;
    var ffmpegCommand = '/vagrant/ffmpeg/ffmpeg -i "' + video + '" -vf scale=iw*sar:ih -q:v 1 "' + formatDir(path) + '/%03d.jpg"'
    console.log('Splitting Video\n');
    if (typeof path === 'function') {
        callback = path;
    }
    console.log(ffmpegCommand);
    
    process = exec(ffmpegCommand,
        function(error, stdout, stderr) {
            if (error) {
                console.log('exec error: ' + error);
                callback(arguments);
            } else {
                console.log('finished video split');
                callback(arguments);
            }
        });
}

exports.split = split;
