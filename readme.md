## Getting Started

```bash
npm install
npm install -g jspm
jspm install
vagrant box add ubuntu14dev https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box
vagrant up
```

The direct your browser to

```bash
192.168.33.10:3000
```

To rebuild bundle JS

```bash
cd APP_ROOT
jspm bundle javascripts/main.js javascripts/bundle.js
```
