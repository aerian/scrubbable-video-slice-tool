#!/usr/bin/env bash
echo "remove old"

chkconfig --del initscrubbabled
rm /etc/init.d/initscrubbabled
rm /etc/init/initscrubbabled

echo "install new"
cp /vagrant/initscrubbabled /etc/init/
chmod a+x /etc/init/initscrubbabled
ln -s /etc/init/initscrubbabled /etc/init.d/initscrubbabled
chkconfig --add initscrubbabled

