var express = require('express'),
    router = express.Router(),
    request = require('request'),
    config = require('../modules/config.js'),
    multer = require('multer'),
    md5 = require('md5'),
    // fs = require('fs'),
    upload = multer({ dest: 'uploads/'}),
    http = require('http'),
    server = http.createServer(express),
    io = require('socket.io').listen(server),
    app = require('../app.js'),
    Scrub = require('../modules/Scrub.js');

// console.log(global);

router.all('*', function(req, res, next){
    if(process.env.NODE_ENV === 'production') {
        app.locals = app.locals || {};
        app.locals.production = true;
    }
    next();
})

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: config.title });
});
router.get('/test', function(res, res, next) {
    res.render('test');
});


router.get('/pack-is-ready/:id', (req, res, next) => {
        var path = req.protocol + '://' + req.get('host') + '/' + req.params.id + '/scrubbable-pack.zip';


       if (router.scrub.done) {
          res.json({status: 200, path: path});
       } else {
            res.json({status: 404, path: path});
       }

});


router.post('/upload', upload.single('video'), function(req, res, next) {
    if (!req.file || !req.file.mimetype.match(/video/)) {
        res.render('index', {title: config.title, errors: [config.error.filetype]});
    } else {
        router.scrub = new Scrub();


        router.scrub.start(req.file.path);

        res.render('waiting', {uploadId: router.scrub.id});
    }



});

module.exports = router;
