#!/usr/bin/env bash


apt-get update
add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main"
add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"
apt-get install -y git-core curl
apt-get install build-essential libssl-dev


# Install ffmpeg
add-apt-repository -y ppa:kirillshkrogalev/ffmpeg-next
apt-get update
apt-get install -y ffmpeg





# imagemagick

apt-get -y update
apt-get -y install imagemagick

# node, ruby, compass and npm
apt-get -y install node npm ruby-full
npm install npm@3.3.4
gem install compass

# Installing n
npm install -g n
n 4.0.0


npm install forever -g
npm install nodemon -g
npm install jspm -g
cd /vagrant
npm install
jspm install

echo 'share folder contents -----'
ls /vagrant/

# # initialise scrubbable
cp /vagrant/initscrubbabled /etc/init/
chmod a+x /etc/init/initscrubbabled
ln -s /etc/init/initscrubbabled /etc/init.d/initscrubbabled
update-rc.d initscrubbabled defaults

service initscrubbabled start