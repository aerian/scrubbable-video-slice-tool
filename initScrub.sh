#!/bin/sh
echo "start scrub cruncher with node"
# NODE_ENV=production /usr/local/bin/node ./bin/www
forever stopall
killall -9 node
cd /vagrant
NODE_ENV=production forever -c /usr/local/bin/node start -l /vagrant/logs/log+$(date +"%y-%m-%d-%T").txt  ./bin/www
